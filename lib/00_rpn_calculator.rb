class RPNCalculator
  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end


  def plus
    raise "calculator is empty" if @stack.length < 2
    @stack[-2..-1] = @stack[-2..-1].inject(:+)
  end

  def minus
    raise "calculator is empty" if @stack.length < 2
    @stack[-2..-1] = @stack[-2..-1].inject(:-)
  end

  def divide
    raise "calculator is empty" if @stack.length < 2
    @stack[-2..-1] = @stack.map(&:to_f)[-2..-1].inject(:/)
  end

  def times
    raise "calculator is empty" if @stack.length < 2
    @stack[-2..-1] = @stack[-2..-1].inject(:*)
  end

  def tokens(string)
    @tokens = string.split(" ").map do |el|
      "-+/*".include?(el) ? el.to_sym : el.to_i
    end
  end

  def evaluate(string)
    tokens(string)
    @tokens.each do |el|
      case el
      when :+
        plus
      when :-
        minus
      when :/
        divide
      when :*
        times
      else
        @stack << el
      end
    end
    value
  end

  def value
    @stack[-1]
  end
end
